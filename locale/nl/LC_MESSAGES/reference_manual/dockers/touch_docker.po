# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-12 14:19+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/dockers/touch_docker.rst:1
msgid "Overview of the touch docker."
msgstr "Overzicht van de aanraakvastzetter."

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Touch"
msgstr "Aanraking"

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Finger"
msgstr "Vinger"

#: ../../reference_manual/dockers/touch_docker.rst:10
msgid "Tablet UI"
msgstr "Tablet UI"

#: ../../reference_manual/dockers/touch_docker.rst:15
msgid "Touch Docker"
msgstr "Aanraakvastzetter"

#: ../../reference_manual/dockers/touch_docker.rst:17
msgid ""
"The Touch Docker is a QML docker with several convenient actions on it. Its "
"purpose is to aid those who use Krita on a touch-enabled screen by providing "
"bigger gui elements."
msgstr ""
"De aanraakvastzetter is een QML-Vastzetter met erop verschillende nuttige "
"acties. Zijn doel is om diegenen te helpen die Krita gebruiken op een voor "
"aanraken ingeschakeld scherm door grotere gui-elementen te leveren."

#: ../../reference_manual/dockers/touch_docker.rst:19
msgid "Its actions are..."
msgstr "Zijn acties zijn..."

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Open File"
msgstr "Bestand openen"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save File"
msgstr "Bestand opslaan"

#: ../../reference_manual/dockers/touch_docker.rst:22
msgid "Save As"
msgstr "Opslaan als"

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Undo"
msgstr "Ongedaan maken"

#: ../../reference_manual/dockers/touch_docker.rst:24
msgid "Redo"
msgstr "Opnieuw"

#: ../../reference_manual/dockers/touch_docker.rst:26
msgid "Decrease Opacity"
msgstr "Transparantie verminderen"

#: ../../reference_manual/dockers/touch_docker.rst:28
msgid "Increase Opacity"
msgstr "Transparantie verhogen"

#: ../../reference_manual/dockers/touch_docker.rst:30
msgid "Increase Lightness"
msgstr "Helderheid vergroten"

#: ../../reference_manual/dockers/touch_docker.rst:32
msgid "Decrease Lightness"
msgstr "Helderheid verkleinen"

#: ../../reference_manual/dockers/touch_docker.rst:34
msgid "Zoom in"
msgstr "Inzoomen"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Counter Clockwise 15°"
msgstr "Linksom draaien 15°"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Reset Canvas Rotation"
msgstr "Rotatie van werkveld resetten"

#: ../../reference_manual/dockers/touch_docker.rst:36
msgid "Rotate Clockwise 15°"
msgstr "Rechtsom draaien 15°"

#: ../../reference_manual/dockers/touch_docker.rst:38
msgid "Zoom out"
msgstr "Uitzoomen"

#: ../../reference_manual/dockers/touch_docker.rst:40
msgid "Decrease Brush Size"
msgstr "Penseelgrootte verkleinen"

#: ../../reference_manual/dockers/touch_docker.rst:42
msgid "Increase Brush Size"
msgstr "Penseelgrootte vergroten"

#: ../../reference_manual/dockers/touch_docker.rst:44
msgid "Delete Layer Contents"
msgstr "Inhoud van laag verwijderen"
