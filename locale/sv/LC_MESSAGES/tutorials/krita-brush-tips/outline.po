# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-30 09:25+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: bild som demonstrerar lagerstilens fulhack för effekten"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""
".. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: bild som demonstrerar lagerstilens fulhack för effekten"

#: ../../tutorials/krita-brush-tips/outline.rst:1
msgid "A tutorial about painting outline while you draw with brush"
msgstr "En handledning om hur man målar konturer med användning av en pensel"

#: ../../tutorials/krita-brush-tips/outline.rst:13
msgid "Brush-tips:Outline"
msgstr "Penselspetsar: Kontur"

#: ../../tutorials/krita-brush-tips/outline.rst:16
msgid "Question"
msgstr "Fråga"

#: ../../tutorials/krita-brush-tips/outline.rst:18
msgid "How to make an outline for a single brush stroke using Krita?"
msgstr ""
"Hur skapar man en kontur för ett enda penseldrag med användning av Krita?"

#: ../../tutorials/krita-brush-tips/outline.rst:20
msgid ""
"Not really a brush, but what you can do is add a layer style to a layer, by |"
"mouseright| a layer and selecting layer style. Then input the following "
"settings:"
msgstr ""
"Även om det egentligen inte är en pensel, kan man lägga till en lagerstil på "
"ett lager genom att använda höger musknapp på en lagerstil. Mata därefter in "
"följande inställningar:"

#: ../../tutorials/krita-brush-tips/outline.rst:25
msgid ""
"Then, set the main layer to multiply (or add a :ref:`filter_color_to_alpha` "
"filter mask), and paint with white:"
msgstr ""
"Ställ därefter in huvudlagret att multiplicera (eller lägg till en "
"filtermask enligt :ref:`filter_color_to_alpha`) och måla med vitt:"

#: ../../tutorials/krita-brush-tips/outline.rst:30
msgid ""
"(The white thing is the pop-up that you see as you hover over the layer)"
msgstr "(Den vita saken är rutan som man ser när man håller musen över lagret)"

#: ../../tutorials/krita-brush-tips/outline.rst:32
msgid "Merge into a empty clear layer after ward to fix all the effects."
msgstr ""
"Sammanfoga det till ett tomt klarlager efteråt för att fixa alla effekter."
