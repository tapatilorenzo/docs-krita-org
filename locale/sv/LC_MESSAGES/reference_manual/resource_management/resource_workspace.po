# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-01 14:34+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/resource_management/resource_workspace.rst:1
msgid "Managing workspaces and sessions in Krita."
msgstr "Hantera arbetsytor och sessioner i Krita."

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:16
msgid "Workspaces"
msgstr "Arbetsytor"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:25
msgid "Window Layouts"
msgstr "Fönsterlayouter"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:37
msgid "Sessions"
msgstr "Sessioner"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
msgid "Resources"
msgstr "Resurser"

#: ../../reference_manual/resource_management/resource_workspace.rst:18
msgid ""
"Workspaces are basically saved configurations of dockers.  Each workspace "
"saves how the dockers are grouped and where they are placed on the screen.  "
"They allow you to easily move between workflows without having to manual "
"reconfigure your setup each time.  They can be as simple or as complex as "
"you want."
msgstr ""
"Arbetsytor är egentligen sparade panelinställningar. Varje arbetsyta sparar "
"hur paneler är grupperade och var de är placerade på skärmen. De låter dig "
"enkelt byta mellan arbetsflöden utan att manuellt ställa om inställningen "
"varje gång. De kan vara hur enkla eller hur komplicerade som man vill."

#: ../../reference_manual/resource_management/resource_workspace.rst:20
msgid ""
"Workspaces can only be accessed via the toolbar or :menuselection:`Window --"
"> Workspaces` There's no docker for them.  You can save workspaces, in which "
"your current configuration is saved. You can also import them (from a \\*."
"kws file), or delete them (which black lists them)."
msgstr ""
"Arbetsytor kan bara kommas åt via verktygsraden eller :menuselection:"
"`Fönster --> Arbetsytor`. Det finns ingen panel för dem. Man kan spara "
"arbetsytor, där aktuell inställning sparas. Man kan också importera dem "
"(från en \\*.kws-fil), eller ta bort dem (vilket svartlistar dem)."

#: ../../reference_manual/resource_management/resource_workspace.rst:22
msgid ""
"Workspaces can technically be tagged, but outside of the resource manager "
"this is not possible."
msgstr ""
"Arbetsytor kan tekniskt sett etiketteras, men utanför resurshanteraren är "
"det inte möjligt."

#: ../../reference_manual/resource_management/resource_workspace.rst:27
msgid ""
"When you work with multiple screens, a single window with a single workspace "
"won't be enough. For multi monitor setups we instead can use sessions. "
"Window layouts allow us to store multiple windows, their positions and the "
"monitor they were on."
msgstr ""
"När man arbetar med flera skärmar, är inte ett enda fönster med en enstaka "
"arbetsyta tillräckligt. För system med flera bildskärmar kan sessioner "
"användas istället. Fösnterlayouter låter oss lagra flera fönster, deras "
"positioner och bildskärmen de finns på."

#: ../../reference_manual/resource_management/resource_workspace.rst:29
msgid ""
"You can access Window Layouts from the workspace drop-down in the toolbar."
msgstr ""
"Man kan komma åt fönsterlayouter från arbetsytans kombinationsruta i "
"verktygsraden."

#: ../../reference_manual/resource_management/resource_workspace.rst:31
msgid "Primary Workspace Follows Focus"
msgstr "Primär arbetsyta följer fokus"

#: ../../reference_manual/resource_management/resource_workspace.rst:32
msgid ""
"This treats the workspace in the first window as the 'primary' workspace, "
"and when you switch focus, it will switch the secondary windows to that "
"primary workspace. This is useful when the secondary workspace is a very "
"sparse workspace with few dockers, and the primary is one with a lot of "
"different dockers."
msgstr ""
"Det här behandlar arbetsytan i det första fönstret som den 'primära' "
"arbetsytan, och när man byter fokus ändras de sekundära fönstren till den "
"primära arbetsytan. Det är användbart när den sekundära arbetsytan är en "
"gles arbetsyta med få paneler, och den primära är en med en mängd olika "
"paneler."

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid "Show Active Image In All Windows"
msgstr "Visa aktiv bild i alla fönster"

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid ""
"This will synchronise the currently viewed image in all windows. Without it, "
"different windows can open separate views for an image via :menuselection:"
"`Window --> New View --> document.kra`."
msgstr ""
"Det här synkroniserar bilden som för närvarande visas i alla fönster. Utan "
"det kan olika fönster öppna separata vyer av en bild via :menuselection:"
"`Fönster --> Ny vy --> dokument.kra`."

#: ../../reference_manual/resource_management/resource_workspace.rst:39
msgid ""
"Sessions allow Krita to store the images and windows opened. You can tell "
"Krita to automatically save current or recover previous sessions if so "
"configured in the :ref:`misc_settings`."
msgstr ""
"Sessioner låter Krita lagra öppna bilder och fönster. Man kan tala om för "
"Krita att automatiskt spara den aktuella sessionen eller återställa tidigare "
"om det är inställt under :ref:`misc_settings`."

#: ../../reference_manual/resource_management/resource_workspace.rst:41
msgid "You can access sessions from :menuselection:`File --> Sessions`."
msgstr "Man kan komma åt sessioner från :menuselection:`Arkiv --> Sessioner`."
