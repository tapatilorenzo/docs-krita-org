# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 18:28+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/filters/emboss.rst:1
msgid "Overview of the emboss filters."
msgstr "Översikt av relieffiltren."

#: ../../reference_manual/filters/emboss.rst:10
#: ../../reference_manual/filters/emboss.rst:15
msgid "Emboss"
msgstr "Relief"

#: ../../reference_manual/filters/emboss.rst:10
msgid "Filters"
msgstr "Filter"

#: ../../reference_manual/filters/emboss.rst:17
msgid ""
"Filters that are named by the traditional embossing technique. This filter "
"generates highlight and shadows to create an effect which makes the image "
"look like embossed. Emboss filters are usually used in the creation of "
"interesting GUI elements, and mostly used in combination with filter-layers "
"and masks."
msgstr ""
"Filter som namnges av den traditionella relieftekniken. Det här filtret "
"generar dagrar och skuggar för att skapa en effekt som gör att bilden ser ut "
"att ha relief. Relieffilter används oftast när intressanta element i "
"grafiska användargränssnitt skapas, och används oftast tillsammans med "
"filterlager och masker."

#: ../../reference_manual/filters/emboss.rst:20
msgid "Emboss Horizontal Only"
msgstr "Relief enbart horisontellt"

#: ../../reference_manual/filters/emboss.rst:22
msgid "Only embosses horizontal lines."
msgstr "Lägger bara till relief för horisontella linjer."

#: ../../reference_manual/filters/emboss.rst:25
msgid "Emboss in all Directions"
msgstr "Relief i alla riktningar"

#: ../../reference_manual/filters/emboss.rst:27
msgid "Embosses in all possible directions."
msgstr "Lägger till relief i alla möjliga riktningar."

#: ../../reference_manual/filters/emboss.rst:30
msgid "Emboss (Laplacian)"
msgstr "Relief (Laplacetransform)"

#: ../../reference_manual/filters/emboss.rst:32
msgid "Uses the laplacian algorithm to perform embossing."
msgstr "Använder Laplacealgoritmen för att utföra relief."

#: ../../reference_manual/filters/emboss.rst:35
msgid "Emboss Vertical Only"
msgstr "Relief enbart vertikalt"

#: ../../reference_manual/filters/emboss.rst:37
msgid "Only embosses vertical lines."
msgstr "Lägger bara till relief för vertikala linjer."

#: ../../reference_manual/filters/emboss.rst:40
msgid "Emboss with Variable depth"
msgstr "Relief med variabelt djup"

#: ../../reference_manual/filters/emboss.rst:42
msgid ""
"Embosses with a depth that can be set through the dialog box shown below."
msgstr ""
"Lägger till relief med ett djup som kan ställas in via dialogrutan som visas "
"nedan."

#: ../../reference_manual/filters/emboss.rst:45
msgid ".. image:: images/filters/Emboss-variable-depth.png"
msgstr ".. image:: images/filters/Emboss-variable-depth.png"

#: ../../reference_manual/filters/emboss.rst:47
msgid "Emboss Horizontal and Vertical"
msgstr "Relief horisontellt och vertikalt"

#: ../../reference_manual/filters/emboss.rst:49
msgid "Only embosses horizontal and vertical lines."
msgstr "Lägger bara till relief för horisontella och vertikala linjer."
