# Translation of docs_krita_org_general_concepts___file_formats___file_psd.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:44+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_psd.rst:1
msgid "The Photoshop file format as exported by Krita."
msgstr "El format de fitxer del Photoshop tal com l'exporta el Krita."

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "*.psd"
msgstr "*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "PSD"
msgstr "PSD"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "Photoshop Document"
msgstr "Document del Photoshop"

#: ../../general_concepts/file_formats/file_psd.rst:15
msgid "\\*.psd"
msgstr "\\*.psd"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_psd.rst:17
msgid ""
"``.psd`` is Photoshop's internal file format. For some reason, people like "
"to use it as an interchange format, even though it is not designed for this."
msgstr ""
"El ``.psd`` és el format de fitxer intern del Photoshop. Per alguna raó, a "
"la gent li agrada emprar-lo com a un format d'intercanvi, encara que no "
"estigui dissenyat per això."

#: ../../general_concepts/file_formats/file_psd.rst:19
msgid ""
"``.psd``, unlike actual interchange formats like :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` and :ref:`file_svg` doesn't "
"have an official spec online. Which means that it needs to be reverse "
"engineered. Furthermore, as an internal file format, it doesn't have much of "
"a philosophy to its structure, as it's only purpose is to save what "
"Photoshop is busy with, or rather, what all the past versions of Photoshop "
"have been busy with. This means that the inside of a PSD looks somewhat like "
"Photoshop's virtual brains, and PSD is in general a very disliked file-"
"format."
msgstr ""
"A diferència dels formats d'intercanvi reals com els :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` i :ref:`file_svg` no té una "
"especificació oficial en línia. El qual vol dir que requereix d'enginyeria "
"inversa. A més, com un format de fitxer intern, no té molta filosofia en la "
"seva estructura, ja que el seu únic propòsit és desar amb el que treballa el "
"Photoshop, o més aviat, amb totes les versions anteriors del Photoshop. Això "
"vol dir que l'interior d'un PSD s'assembla als cervells virtuals del "
"Photoshop, i el PSD en general és un format de fitxer molt desagradable."

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_psd.rst:21
msgid ""
"Due to ``.psd`` being used as an interchange format, this leads to confusion "
"amongst people using these programs, as to why not all programs support "
"opening these. Sometimes, you might even see users saying that a certain "
"program is terrible because it doesn't support opening PSDs properly. But as "
"PSD is an internal file-format without online specs, it is impossible to "
"have any program outside it support it 100%."
msgstr ""
"A causa que el ``.psd`` s'utilitza com un format d'intercanvi, això porta a "
"la confusió entre la gent que utilitza aquests programes, pel que fa a "
"perquè no tots els programes admeten la seva obertura. De vegades, pot ser "
"que fins i tot els usuaris diguin que cert programa és terrible perquè no "
"admet correctament l'obertura dels PSD. Però com el PSD és un format de "
"fitxer intern sense especificacions en línia, és impossible que un programa "
"extern l'admeti al 100%."

#: ../../general_concepts/file_formats/file_psd.rst:23
msgid ""
"Krita supports loading and saving raster layers, blending modes, "
"layerstyles, layer groups, and transparency masks from PSD. It will likely "
"never support vector and text layers, as these are just too difficult to "
"program properly."
msgstr ""
"El Krita admet carregar i desar capes ràster, modes de barreja, estils de la "
"capa, capes de grup i màscares de transparència des dels PSD. És probable "
"que mai admeti capes vectorials i de text, ja que són massa difícils de "
"programar correctament."

#: ../../general_concepts/file_formats/file_psd.rst:25
msgid ""
"We recommend using any other file format instead of PSD if possible, with a "
"strong preference towards :ref:`file_ora` or :ref:`file_tif`."
msgstr ""
"Si és possible, recomanem utilitzar qualsevol altre format de fitxer en lloc "
"del PSD, amb una forta preferència cap a l':ref:`file_ora` o :ref:`file_tif`."

#: ../../general_concepts/file_formats/file_psd.rst:27
msgid ""
"As a working file format, PSDs can be expected to become very heavy and most "
"websites won't accept them."
msgstr ""
"Com un format de fitxer per a treballar, podeu esperar que els PSD siguin "
"molt pesats i la majoria dels llocs web no els accepten."
