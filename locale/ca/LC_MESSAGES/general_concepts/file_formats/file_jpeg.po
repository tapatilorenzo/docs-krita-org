# Translation of docs_krita_org_general_concepts___file_formats___file_jpeg.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 13:41+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/file_formats/file_jpeg.rst:1
msgid "The JPEG file format as exported by Krita."
msgstr "El format de fitxer JPEG tal com l'exporta el Krita."

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpeg"
msgstr "jpeg"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "jpg"
msgstr "jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:10
msgid "*.jpg"
msgstr "*.jpg"

#: ../../general_concepts/file_formats/file_jpeg.rst:16
msgid "\\*.jpg"
msgstr "\\*.jpg"

# skip-rule: t-acc_obe
#: ../../general_concepts/file_formats/file_jpeg.rst:18
msgid ""
"``.jpg``, ``.jpeg`` or ``.jpeg2000`` are a family of file-formats designed "
"to encode photographs."
msgstr ""
"Els ``.jpg``, ``.jpeg`` o ``.jpeg2000`` són una família de formats de "
"fitxers dissenyats per a codificar les fotografies."

#: ../../general_concepts/file_formats/file_jpeg.rst:20
msgid ""
"Photographs have the problem that they have a lot of little gradients, which "
"means that you cannot index the file like you can with :ref:`file_gif` and "
"expect the result to look good. What JPEG instead does is that it converts "
"the file to a perceptual color space (:ref:`YCrCb <model_ycrcb>`), and then "
"compresses the channels that encode the colors, while keeping the channel "
"that holds information about the relative lightness uncompressed. This works "
"really well because human eye-sight is not as sensitive to colorfulness as "
"it is to relative lightness. JPEG also uses other :ref:`lossy "
"<lossy_compression>` compression techniques, like using cosine waves to "
"describe image contrasts."
msgstr ""
"Les fotografies tenen el problema que tenen molt pocs degradats, el qual vol "
"dir que no podreu crear un índex del fitxer com podeu fer-ho amb el :ref:"
"`file_gif` i esperar que el resultat sigui bo. El que fa el JPEG és que "
"converteix el fitxer en un espai de color perceptiu (:ref:`YCrCb "
"<model_ycrcb>`) i comprimeix els canals que codifiquen els colors, mantenint "
"sense comprimir el canal que conté la informació relativa a la claredat. "
"Això funciona molt bé, ja que la vista dels ulls humans no és tan sensible a "
"la coloració com a la claredat relativa. El JPEG també utilitza altres "
"tècniques de compressió :ref:`amb pèrdua <lossy_compression>`, com utilitzar "
"ones cosinus per a descriure els contrastos de la imatge."

#: ../../general_concepts/file_formats/file_jpeg.rst:22
msgid ""
"However, it does mean that JPEG should be used in certain cases. For images "
"with a lot of gradients, like full scale paintings, JPEG performs better "
"than :ref:`file_png` and :ref:`file_gif`."
msgstr ""
"Tanmateix, vol dir que el JPEG s'hauria d'utilitzar en determinats casos. "
"Per a imatges amb molts degradats, com ara pintures a escala completa, en "
"aquests casos el JPEG té un millor rendiment que el :ref:`file_png` i el :"
"ref:`file_gif`."

#: ../../general_concepts/file_formats/file_jpeg.rst:24
msgid ""
"But for images with a lot of sharp contrasts, like text and comic book "
"styles, PNG is a much better choice despite a larger file size. For "
"grayscale images, :ref:`file_png` and :ref:`file_gif` will definitely be "
"more efficient."
msgstr ""
"Però, per a les imatges amb molts contrastos més definits, com ara estils de "
"text i de còmic, el PNG és una opció molt millor malgrat una mida de fitxer "
"més gran. Per a les imatges en escala de grisos, els :ref:`file_png` i :ref:"
"`file_gif` definitivament seran més eficients."

#: ../../general_concepts/file_formats/file_jpeg.rst:26
msgid ""
"Because JPEG uses lossy compression, it is not advised to save over the same "
"JPEG multiple times. The lossy compression will cause the file to reduce in "
"quality each time you save it. This is a fundamental problem with lossy "
"compression methods. Instead use a lossless file format, or a working file "
"format while you are working on the image."
msgstr ""
"Com que el JPEG utilitza la compressió amb pèrdua, no es recomana desar el "
"mateix JPEG múltiples vegades. La compressió amb pèrdua farà que el fitxer "
"redueixi la qualitat cada vegada que el deseu. Aquest és un problema "
"fonamental amb els mètodes de compressió amb pèrdua. En canvi, utilitzeu un "
"format de fitxer sense pèrdua o un format de fitxer per a treballar mentre "
"esteu treballant en la imatge."
