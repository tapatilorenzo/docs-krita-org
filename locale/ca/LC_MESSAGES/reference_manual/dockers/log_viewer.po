# Translation of docs_krita_org_reference_manual___dockers___log_viewer.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 20:32+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "Configure Logging"
msgstr "Configurar el registre"

#: ../../reference_manual/dockers/log_viewer.rst:1
msgid "Overview of the log viewer docker."
msgstr "Vista general de l'acoblador Visor del registre."

#: ../../reference_manual/dockers/log_viewer.rst:10
#: ../../reference_manual/dockers/log_viewer.rst:16
msgid "Log Viewer"
msgstr "Visor del registre"

#: ../../reference_manual/dockers/log_viewer.rst:10
msgid "Debug"
msgstr "Depuració"

#: ../../reference_manual/dockers/log_viewer.rst:18
msgid ""
"The log viewer docker allows you to see debug output without access to a "
"terminal. This is useful when trying to get a tablet log or to figure out if "
"Krita is spitting out errors while a certain thing is happening."
msgstr ""
"L'acoblador Visor del registre permet veure la sortida de depuració sense "
"accedir a un terminal. Això és útil quan es mira d'obtenir un registre de la "
"tauleta o d'esbrinar si el Krita està mostrant errors mentre succeeix "
"quelcom."

#: ../../reference_manual/dockers/log_viewer.rst:20
msgid ""
"The log docker is used by pressing the :guilabel:`enable logging` button at "
"the bottom."
msgstr ""
"Aquest acoblador s'utilitza prement el botó :guilabel:`Activa el registre` "
"que hi ha a la part inferior."

#: ../../reference_manual/dockers/log_viewer.rst:24
msgid ""
"When enabling logging, this output will not show up in the terminal. If you "
"are missing debug output in the terminal, check that you didn't have the log "
"docker enabled."
msgstr ""
"En habilitar el registre, aquesta sortida no es mostrarà en el terminal. Si "
"requeriu la sortida de depuració en el terminal, comproveu que no heu "
"habilitat aquest acoblador."

#: ../../reference_manual/dockers/log_viewer.rst:26
msgid ""
"The docker is composed of a log area which shows the debug output, and four "
"buttons at the bottom."
msgstr ""
"L'acoblador està compost per una àrea de registre, la qual mostrarà la "
"sortida de depuració amb quatre botons a la part inferior."

#: ../../reference_manual/dockers/log_viewer.rst:29
msgid "Log Output Area"
msgstr "Àrea de sortida del registre"

#: ../../reference_manual/dockers/log_viewer.rst:31
msgid "The log output is formatted as follows:"
msgstr "La sortida del registre està ve amb el format següent:"

#: ../../reference_manual/dockers/log_viewer.rst:33
msgid "White"
msgstr "Blanc"

#: ../../reference_manual/dockers/log_viewer.rst:34
msgid "This is just a regular debug message."
msgstr "Aquest només és un missatge de depuració normal."

#: ../../reference_manual/dockers/log_viewer.rst:35
msgid "Yellow"
msgstr "Groc"

#: ../../reference_manual/dockers/log_viewer.rst:36
msgid "This is a info output."
msgstr "Aquesta és una sortida informativa."

#: ../../reference_manual/dockers/log_viewer.rst:37
msgid "Orange"
msgstr "Taronja"

#: ../../reference_manual/dockers/log_viewer.rst:38
msgid "This is a warning output."
msgstr "Aquesta és una sortida d'avís."

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "Red"
msgstr "Vermell"

#: ../../reference_manual/dockers/log_viewer.rst:40
msgid "This is a critical error. When this is bolded, it is a fatal error."
msgstr ""
"Aquest és un error crític. Quan es mostra en negreta, és un error fatal."

#: ../../reference_manual/dockers/log_viewer.rst:43
msgid "Options"
msgstr "Opcions"

#: ../../reference_manual/dockers/log_viewer.rst:45
msgid "There's four buttons at the bottom:"
msgstr "Hi ha quatre botons a la part inferior:"

#: ../../reference_manual/dockers/log_viewer.rst:47
msgid "Enable Logging"
msgstr "Activa el registre"

#: ../../reference_manual/dockers/log_viewer.rst:48
msgid "Enable the docker to start logging. This caries over between sessions."
msgstr ""
"Habilita l'acoblador per iniciar el registre. Això s'ha de fer entre les "
"sessions."

#: ../../reference_manual/dockers/log_viewer.rst:49
msgid "Clear the Log"
msgstr "Neteja el registre"

#: ../../reference_manual/dockers/log_viewer.rst:50
msgid "This empties the log output area."
msgstr "Això buida l'àrea de sortida del registre."

#: ../../reference_manual/dockers/log_viewer.rst:51
msgid "Save the Log"
msgstr "Desa el registre"

#: ../../reference_manual/dockers/log_viewer.rst:52
msgid "Save the log to a text file."
msgstr "Desa el registre en un fitxer de text."

#: ../../reference_manual/dockers/log_viewer.rst:54
msgid ""
"Configure which kind of debug is added. By default only warnings and simple "
"debug statements are logged. You can enable the special debug messages for "
"each area here."
msgstr ""
"Configureu quin tipus de depuració afegir. De manera predeterminada només "
"s'enregistraran els avisos i declaracions de depuració senzilles. Des d'aquí "
"podreu habilitar els missatges de depuració especials per a cada àrea."

#: ../../reference_manual/dockers/log_viewer.rst:56
msgid "General"
msgstr "General"

#: ../../reference_manual/dockers/log_viewer.rst:57
msgid "Resource Management"
msgstr "Gestió de recursos"

#: ../../reference_manual/dockers/log_viewer.rst:58
msgid "Image Core"
msgstr "Nucli de la imatge"

#: ../../reference_manual/dockers/log_viewer.rst:59
msgid "Registries"
msgstr "Registres"

#: ../../reference_manual/dockers/log_viewer.rst:60
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/dockers/log_viewer.rst:61
msgid "Tile Engine"
msgstr "Motor del mosaic"

#: ../../reference_manual/dockers/log_viewer.rst:62
msgid "Filters"
msgstr "Filtres"

#: ../../reference_manual/dockers/log_viewer.rst:63
msgid "Plugin Management"
msgstr "Gestió dels connectors"

#: ../../reference_manual/dockers/log_viewer.rst:64
msgid "User Interface"
msgstr "Interfície d'usuari"

#: ../../reference_manual/dockers/log_viewer.rst:65
msgid "File Loading and Saving"
msgstr "Carregar i desar fitxers"

#: ../../reference_manual/dockers/log_viewer.rst:66
msgid "Mathematics and Calculations"
msgstr "Matemàtiques i càlculs"

#: ../../reference_manual/dockers/log_viewer.rst:67
msgid "Image Rendering"
msgstr "Renderitzat de la imatge"

#: ../../reference_manual/dockers/log_viewer.rst:68
msgid "Scripting"
msgstr "Crear scripts"

#: ../../reference_manual/dockers/log_viewer.rst:69
msgid "Input Handling"
msgstr "Maneig de l'entrada"

#: ../../reference_manual/dockers/log_viewer.rst:70
msgid "Actions"
msgstr "Accions"

#: ../../reference_manual/dockers/log_viewer.rst:71
msgid "Tablet Handing"
msgstr "Maneig de la tauleta"

#: ../../reference_manual/dockers/log_viewer.rst:72
msgid "GPU Canvas"
msgstr "Llenç amb la GPU"

#: ../../reference_manual/dockers/log_viewer.rst:73
msgid "Metadata"
msgstr "Metadades"

#: ../../reference_manual/dockers/log_viewer.rst:74
msgid "Color Management"
msgstr "Gestió del color"
