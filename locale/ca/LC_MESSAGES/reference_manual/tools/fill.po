# Translation of docs_krita_org_reference_manual___tools___fill.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 16:28+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<generated>:1
msgid "Use Pattern"
msgstr "Usa el patró"

#: ../../<rst_epilog>:56
msgid ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: toolfill"
msgstr ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: eina d'emplenat"

#: ../../reference_manual/tools/fill.rst:1
msgid "Krita's fill tool reference."
msgstr "Referència de l'eina d'emplenat del Krita."

#: ../../reference_manual/tools/fill.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/fill.rst:11
msgid "Fill"
msgstr "Emplenat"

#: ../../reference_manual/tools/fill.rst:11
msgid "Bucket"
msgstr "Cubeta"

#: ../../reference_manual/tools/fill.rst:16
msgid "Fill Tool"
msgstr "Eina d'emplenat"

#: ../../reference_manual/tools/fill.rst:18
msgid "|toolfill|"
msgstr "|toolfill|"

#: ../../reference_manual/tools/fill.rst:20
msgid ""
"Krita has one of the most powerful and capable Fill functions available. The "
"options found in the Tool Options docker and outlined below will give you a "
"great deal of flexibility working with layers and selections."
msgstr ""
"El Krita té una de les funcions d'emplenament més potent i capaç disponible. "
"Les opcions que es troben a l'acoblador Opcions de l'eina i que es descriuen "
"a continuació us oferiran una gran flexibilitat en treballar amb capes i "
"seleccions."

#: ../../reference_manual/tools/fill.rst:22
msgid ""
"To get started, clicking anywhere on screen with the fill-tool will allow "
"that area to be filed with the foreground color."
msgstr ""
"Per a començar, feu clic a qualsevol lloc de la pantalla i l'eina "
"d'emplenament permetrà que aquesta àrea s'empleni amb el color de primer pla."

#: ../../reference_manual/tools/fill.rst:25
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/fill.rst:27
msgid "Fast Mode"
msgstr "Mode ràpid"

#: ../../reference_manual/tools/fill.rst:28
msgid ""
"This is a special mode for really fast filling. However, many functions "
"don't work with this mode."
msgstr ""
"Aquest és una mode especial per a un emplenament realment ràpid. No obstant "
"això, moltes funcions no funcionaran amb aquest mode."

#: ../../reference_manual/tools/fill.rst:29
msgid "Threshold"
msgstr "Llindar"

#: ../../reference_manual/tools/fill.rst:30
msgid "Determines when the fill-tool sees another color as a border."
msgstr ""
"Determina quan l'eina d'emplenament veurà un altre color com a una vora."

#: ../../reference_manual/tools/fill.rst:31
msgid "Grow Selection"
msgstr "Fes créixer la selecció"

#: ../../reference_manual/tools/fill.rst:32
msgid "This value extends the shape beyond its initial size."
msgstr "Aquest valor estén la forma més enllà de la seva mida inicial."

#: ../../reference_manual/tools/fill.rst:33
msgid "Feathering Radius"
msgstr "Radi de selecció suau"

#: ../../reference_manual/tools/fill.rst:34
msgid "This value will add a soft border to the filled-shape."
msgstr "Aquest valor afegirà una vora suau a la forma de l'emplenat."

#: ../../reference_manual/tools/fill.rst:35
msgid "Fill Entire Selection"
msgstr "Emplena completament la selecció"

#: ../../reference_manual/tools/fill.rst:36
msgid ""
"Activating this will result in the shape filling the whole of the active "
"selection, regardless of threshold."
msgstr ""
"Activar això resultarà en que la forma emplenarà tota la selecció activa, "
"independentment del llindar."

#: ../../reference_manual/tools/fill.rst:37
msgid "Limit to current layer"
msgstr "Limita a la capa actual"

#: ../../reference_manual/tools/fill.rst:38
msgid ""
"Activating this will prevent the fill tool from taking other layers into "
"account."
msgstr ""
"Activar això evitarà que l'eina d'emplenat tingui en compte les altres capes."

#: ../../reference_manual/tools/fill.rst:40
msgid "Ticking this will result in the active pattern being used."
msgstr "Activar això resultarà en que s'utilitzarà el patró actiu."
