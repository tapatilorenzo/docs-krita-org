msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_tutorials___krita-"
"brush-tips___bokeh-brush.pot\n"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_01.png\n"
"   :alt: krita bokeh brush setup background"
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_02.png\n"
"   :alt: Krita bokeh brush tips scatter settings"
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_03.png\n"
"   :alt: Choosing the brush tip for the bokeh effect"
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_04.png\n"
"   :alt: paint the bokeh circles on the background"
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:1
msgid "Creating bokeh effect with the help of some simple brush tip."
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:13
msgid "Brush Tips: Bokeh"
msgstr "笔刷技巧：散景高光"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:16
msgid "Question"
msgstr "问题"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:18
msgid "How do you do bokeh effects?"
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:20
msgid "First, blur your image with the Lens Blur to roughly 50 pixels."
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:26
msgid "Take smudge_textured, add scattering, turn off tablet input."
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:31
msgid ""
"Change the brush-tip to ‘Bokeh’ and check ‘overlay’ (you will want to play "
"with the spacing as well)."
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:36
msgid ""
"Then make a new layer over your drawing, set that to ‘lighter color’ (it’s "
"under lighter category) and painter over it with you brush."
msgstr ""

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:41
msgid ""
"Overlay mode on the smudge brush allows you to sample all the layers, and "
"the ‘lighter color’ blending mode makes sure that the Bokeh circles only "
"show up when they are a lighter color than the original pixels underneath. "
"You can further modify this brush by adding a ‘fuzzy’ sensor to the spacing "
"and size options, changing the brush blending mode to ‘addition’, or by "
"choosing a different brush-tip."
msgstr ""
