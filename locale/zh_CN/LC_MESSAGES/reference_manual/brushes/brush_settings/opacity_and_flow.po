msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_settings___opacity_and_flow."
"pot\n"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:1
msgid "Opacity and flow in Krita."
msgstr "Krita 中的不透明度和流量选项。"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:27
msgid "Opacity"
msgstr "不透明度"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid "Flow"
msgstr "流量"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:12
msgid "Transparency"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:17
msgid "Opacity and Flow"
msgstr "不透明度和流量"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:19
msgid "Opacity and flow are parameters for the transparency of a brush."
msgstr "不透明度和流量都是用来控制笔刷透明度的选项。"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:22
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Flow.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:23
msgid "They are interlinked with the painting mode setting."
msgstr ""
"它们各自与选项列表的中的“绘画模式”的其中一种互联，效果有着微妙却又是本质上的"
"区别。下图从左到右依次展示了绘画模式为“冲刷”时，不透明度为 0.5、流量为 0.5、"
"两者同为 0.5 时的笔画效果区别。"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:26
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_02.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:28
msgid "The transparency of a stroke."
msgstr "控制一条笔画整体的透明度。"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:30
msgid ""
"The transparency of separate dabs. Finally separated from Opacity in 2.9."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:34
msgid ".. image:: images/brushes/Krita_4_2_brushengine_opacity-flow_01.svg"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:35
msgid ""
"In Krita 4.1 and below, the flow and opacity when combined with brush "
"sensors would add up to one another, being only limited by the maximum "
"opacity. This was unexpected compared to all other painting applications, so "
"in 4.2 this finally got corrected to the flow and opacity multiplying, "
"resulting in much more subtle strokes. This change can be switched back in "
"the :ref:`tool_options_settings`, but we will be deprecating the old way in "
"future versions."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:38
msgid "The old behavior can be simulated in the new system by..."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:40
msgid "Deactivating the sensors on opacity."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:41
msgid "Set the maximum value on flow to 0.5."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:42
msgid "Adjusting the pressure curve to be concave."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:44
msgid ".. image:: images/brushes/flow_opacity_adapt_flow_preset.gif"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:49
msgid "Painting mode"
msgstr "绘画模式"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:51
msgid "Build-up"
msgstr "堆积"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:52
msgid "Will treat opacity as if it were the same as flow."
msgstr ""
"按照前面所讲的“流量”的方式处理透明度。在一条笔画内部互相重叠的笔尖印迹会加"
"深。"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Wash"
msgstr "冲刷"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:54
msgid "Will treat opacity as stroke transparency instead of dab-transparency."
msgstr ""
"按照前面所讲的“不透明度”的方式来处理笔画的透明度。整条笔画的透明度是均匀一致"
"的。"

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:57
msgid ".. image:: images/brushes/Krita_2_9_brushengine_opacity-flow_03.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/opacity_and_flow.rst:58
msgid ""
"Where the other images of this page had all three strokes set to painting "
"mode: wash, this one is set to build-up."
msgstr ""
