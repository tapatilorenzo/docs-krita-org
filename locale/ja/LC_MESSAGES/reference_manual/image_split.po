msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-05-04 11:06-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../<generated>:1
msgid "Autosave on split"
msgstr ""

#: ../../reference_manual/image_split.rst:1
msgid "The Image Split functionality in Krita"
msgstr ""

#: ../../reference_manual/image_split.rst:10
msgid "Splitting"
msgstr ""

#: ../../reference_manual/image_split.rst:15
msgid "Image Split"
msgstr ""

#: ../../reference_manual/image_split.rst:17
msgid ""
"Found under :menuselection:`Image --> Image Split`, the Image Split function "
"allows you to evenly split a document up into several sections. This is "
"useful for splitting up spritesheets for example."
msgstr ""

#: ../../reference_manual/image_split.rst:19
msgid "Horizontal Lines"
msgstr ""

#: ../../reference_manual/image_split.rst:20
msgid ""
"The amount of horizontal lines to split at. 4 lines will mean that the image "
"is split into 5 horizontal stripes."
msgstr ""

#: ../../reference_manual/image_split.rst:22
msgid "Vertical Lines"
msgstr ""

#: ../../reference_manual/image_split.rst:22
msgid ""
"The amount of vertical lines to split at. 4 lines will mean that the image "
"is split into 5 vertical stripes."
msgstr ""

#: ../../reference_manual/image_split.rst:24
msgid "Sort Direction"
msgstr ""

#: ../../reference_manual/image_split.rst:28
msgid "Whether to number the files using the following directions:"
msgstr ""

#: ../../reference_manual/image_split.rst:30
msgid "Horizontal"
msgstr ""

#: ../../reference_manual/image_split.rst:31
msgid "Left to right, top to bottom."
msgstr ""

#: ../../reference_manual/image_split.rst:33
msgid "Vertical"
msgstr ""

#: ../../reference_manual/image_split.rst:33
msgid "Top to bottom, left to right."
msgstr ""

#: ../../reference_manual/image_split.rst:35
msgid "Prefix"
msgstr ""

#: ../../reference_manual/image_split.rst:36
msgid ""
"The prefix at which the files should be saved at. By default this is the "
"current document name."
msgstr ""

#: ../../reference_manual/image_split.rst:37
msgid "File Type"
msgstr ""

#: ../../reference_manual/image_split.rst:38
msgid "Which file format to save to."
msgstr ""

#: ../../reference_manual/image_split.rst:40
msgid ""
"This will result in all slices being saved automatically using the above "
"prefix. Otherwise Krita will ask the name for each slice."
msgstr ""
