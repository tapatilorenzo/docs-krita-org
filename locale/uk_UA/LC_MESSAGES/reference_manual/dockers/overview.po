# Translation of docs_krita_org_reference_manual___dockers___overview.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___dockers___overview\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-05 08:17+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/overview.rst:1
msgid "Overview of the overview docker."
msgstr "Огляд бічної панелі огляду."

#: ../../reference_manual/dockers/overview.rst:11
#: ../../reference_manual/dockers/overview.rst:16
msgid "Overview"
msgstr "Огляд"

#: ../../reference_manual/dockers/overview.rst:11
msgid "Navigation"
msgstr "Навігація"

#: ../../reference_manual/dockers/overview.rst:19
msgid ".. image:: images/dockers/Krita_Overview_Docker.png"
msgstr ".. image:: images/dockers/Krita_Overview_Docker.png"

#: ../../reference_manual/dockers/overview.rst:20
msgid ""
"This docker allows you to see a full overview of your image. You can also "
"use it to navigate and zoom in and out quickly. Dragging the view-rectangle "
"allows you quickly move the view."
msgstr ""
"За допомогою цієї бічної панелі ви зможете бачити загальний огляд "
"зображення. Ви можете скористатися нею для пришвидшення навігації та зміни "
"масштабу зображення. Перетягування прямокутника області перегляду на панелі "
"допоможе вам у швидкому пересуванні області перегляду."

#: ../../reference_manual/dockers/overview.rst:22
msgid ""
"There are furthermore basic navigation functions: Dragging the zoom-slider "
"allows you quickly change the zoom."
msgstr ""
"Передбачено і додаткові можливості навігації: перетягуванням повзунка "
"масштабу ви можете швидко змінити масштаб."

#: ../../reference_manual/dockers/overview.rst:26
msgid ""
"Toggling the mirror button will allow you to mirror the view of the canvas "
"(but not the full image itself) and dragging the rotate slider allows you to "
"adjust the rotation of the viewport. To reset the rotation, |mouseright| the "
"slider to edit the number, and type '0'."
msgstr ""
"Натискання кнопки віддзеркалення надає вам змогу віддзеркалити полотно (але "
"не саме зображення). Перетягуванням повзунка обертання ви можете скоригувати "
"кут обертання на панелі перегляду. Щоб скинути обертання до початкового, "
"клацніть |mouseright| на повзунку, щоб редагувати число, і введіть «0» (без "
"лапок)."
